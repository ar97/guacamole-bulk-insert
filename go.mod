module gitlab.com/ar97/guac-bulk-insert

require (
	github.com/gorilla/mux v1.7.4
	github.com/lib/pq v1.7.0
	github.com/manifoldco/promptui v0.7.0
	github.com/mattn/go-colorable v0.1.2 // indirect
	github.com/mattn/go-isatty v0.0.9 // indirect
	golang.org/x/sys v0.0.0-20190826190057-c7b8b68b1456 // indirect
	gopkg.in/check.v1 v1.0.0-20190902080502-41f04d3bba15 // indirect
)

go 1.13
